add_executable(vs-thumb_test
  vs-thumb_test.cpp
)
target_link_libraries(vs-thumb_test
  vs-thumb-static
  thumbnailer-static
  ${GST_DEPS_LDFLAGS}
  ${IMG_DEPS_LDFLAGS}
  ${GIO_DEPS_LDFLAGS}
  Qt5::Test
  testutils
  gtest
)
add_dependencies(vs-thumb_test vs-thumb)

add_test(vs-thumb vs-thumb_test)
if (${WORKAROUND_LIBGOMP_STATIC_TLS})
  set_tests_properties(vs-thumb PROPERTIES ENVIRONMENT LD_PRELOAD=libgomp.so.1)
endif()
